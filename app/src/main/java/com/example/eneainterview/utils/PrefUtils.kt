package com.example.eneainterview.utils

import android.content.Context
import android.content.SharedPreferences

class PrefUtils {

    companion object {
        private val PREF_USER_DETAILS = "PREF_USER_DETAILS"
        private val PREF_API_KEY = "PREF_API_KEY"
        private val PREF_OAUTH = "PREF_OAUTH"

        private fun getSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences("APP_PREF", Context.MODE_PRIVATE)
        }

        fun getOauthToken(context: Context): String? {
            return getSharedPreferences(context).getString(PREF_API_KEY, null)
        }

        fun setAccessToken(context: Context, accessToken: String) {
            getSharedPreferences(context).edit().putString(Constants.PREF_ACCESS_TOKEN, accessToken).apply()
        }
    }


}