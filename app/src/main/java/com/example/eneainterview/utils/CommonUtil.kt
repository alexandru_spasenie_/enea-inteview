package com.example.eneainterview.utils

import android.app.Dialog
import android.content.Context
import com.example.eneainterview.R

object CommonUtil {
    fun showLoadingDialog(context: Context): Dialog {
        val progressBar = Dialog(context)

        progressBar.let {
            it.setContentView(R.layout.progress_dialog)
            it.show()
        }

        return progressBar
    }

}