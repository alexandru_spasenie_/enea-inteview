package com.example.eneainterview.login

import android.content.Context
import com.example.eneainterview.network.ApiClient
import com.example.eneainterview.utils.Constants
import com.example.eneainterview.utils.PrefUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class LoginPresenterImp(var context: Context, var loginView: LoginView) : LoginPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getAccessToken(code: String) {
        loginView.showProgress()
        compositeDisposable.add(
            ApiClient.create(context)
                .getAccessToken(Constants.githubClientId, Constants.githubClientSecret, code, Constants.redirectUrl)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(object : DisposableSingleObserver<String>() {
                    override fun onSuccess(accessTokenResponse: String) {
                        loginView.hideProgress()
                        PrefUtils.setAccessToken(context, accessTokenResponse)
                        loginView.nextScreen()
                    }

                    override fun onError(e: Throwable) {
                        loginView.hideProgress()
                    }

                })
        )
    }
}