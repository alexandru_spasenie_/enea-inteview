package com.example.eneainterview.login

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.eneainterview.R
import com.example.eneainterview.base.BaseActivity
import com.example.eneainterview.repositories.RepositoriesActivity
import com.example.eneainterview.utils.Constants
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*


class LoginActivity : BaseActivity(), LoginView {
    var TAG = "TOKENOID"


    var OAUTH_URL = "https://github.com/login/oauth/authorize"
    var OAUTH_ACCESS_TOKEN_URL = "https://github.com/login/oauth/access_token"

    var CLIENT_ID = Constants.githubClientId
    var CLIENT_SECRET = Constants.githubClientSecret
    var CALLBACK_URL = "http://localhost"
    lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)

        loginPresenter = LoginPresenterImp(this, this)

        val url = "$OAUTH_URL?client_id=$CLIENT_ID"

        webview.settings.javaScriptEnabled = true
        webview.clearCache(true)
        webview.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                val accessTokenFragment = "access_token="
                val accessCodeFragment = "code="

                // We hijack the GET request to extract the OAuth parameters

                if (url.contains(accessTokenFragment)) {
                    // the GET request contains directly the token
                    val accessToken = url.substring(url.indexOf(accessTokenFragment))
                    //TokenStorer.setAccessToken(accessToken)
                    System.out.println("accessToken=$accessToken")

                } else if (url.contains(accessCodeFragment)) {
                    // the GET request contains an authorization code
                    val accessCode = url.substring(url.indexOf(accessCodeFragment)).replace(accessCodeFragment, "")
                    //TokenStorer.setAccessCode(accessCode)
                    System.out.println("accessCode=$accessCode")

                    val query = "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&$accessCode"
                    view.postUrl(OAUTH_ACCESS_TOKEN_URL, query.toByteArray())
                    loginPresenter.getAccessToken(accessCode)
                }

            }


        }
        webview.loadUrl(url)
    }

    override fun nextScreen() {
        finish()
        startActivity(Intent(this, RepositoriesActivity::class.java))
    }

}
