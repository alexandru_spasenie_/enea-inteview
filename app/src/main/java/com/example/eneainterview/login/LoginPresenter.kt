package com.example.eneainterview.login

interface LoginPresenter {
    fun getAccessToken(code: String)
}