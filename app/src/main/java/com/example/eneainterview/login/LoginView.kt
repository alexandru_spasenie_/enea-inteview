package com.example.eneainterview.login

import com.bookup.app.base.MVPView

interface LoginView : MVPView{
    fun nextScreen()
}