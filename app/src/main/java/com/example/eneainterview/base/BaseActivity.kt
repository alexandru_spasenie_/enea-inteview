package com.example.eneainterview.base

import android.app.Dialog
import android.content.BroadcastReceiver
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bookup.app.base.MVPView
import com.example.eneainterview.R
import com.example.eneainterview.utils.CommonUtil.showLoadingDialog

open class BaseActivity : AppCompatActivity(), MVPView {

    private var progressDialog: Dialog? = null
    private var broadcastReceiver: BroadcastReceiver? = null

    override fun showProgress() {
        if (progressDialog != null && progressDialog?.isShowing!!) {
            return
        }
        hideProgress()
        progressDialog = showLoadingDialog(this)
        progressDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        progressDialog?.setContentView(R.layout.progress_dialog)
        progressDialog?.setCancelable(false)
    }

    override fun hideProgress() {
        progressDialog?.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}