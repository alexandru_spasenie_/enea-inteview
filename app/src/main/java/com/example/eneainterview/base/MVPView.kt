package com.bookup.app.base

interface MVPView {

    fun showProgress()
    fun hideProgress()
}