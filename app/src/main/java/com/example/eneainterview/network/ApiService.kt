package com.example.eneainterview.network

import com.example.eneainterview.repositories.RepositoriesModel
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    @POST("https://github.com/login/oauth/access_token")
    fun getAccessToken(@Query("client_id") clientId : String,
                       @Query("client_secret") client_secret : String,
                       @Query("code") code : String,
                       @Query("redirect_uri") redirect_uri : String) : Single<String>

    @GET("search/repositories?q=topic:ruby+topic:rails")
    fun getRepositories() : Single<RepositoriesModel>

}