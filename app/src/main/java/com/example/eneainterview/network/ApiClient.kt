package com.example.eneainterview.network

import android.content.Context
import com.example.eneainterview.utils.Constants
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder
import com.google.gson.Gson



class ApiClient {

    companion object{
        private var retrofit: Retrofit? = null
        private var okHttpClient: OkHttpClient? = null
        private val TAG = "ApiClient"

        fun getClient(context: Context): Retrofit? {

            if (okHttpClient == null)
                initOkHttp(context)

            val gson = GsonBuilder()
                .setLenient()
                .create()

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit
        }

        fun create(context: Context): ApiService {
            if (retrofit != null) {
                return retrofit!!.create(ApiService::class.java)
            } else {
                retrofit = getClient(context)
                return retrofit!!.create(ApiService::class.java)
            }
        }

        private fun initOkHttp(context: Context) {
            val REQUEST_TIMEOUT = 60
            val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            httpClient.addInterceptor(interceptor)

            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    //.addHeader("Content-Type", "application/json")

                val request = requestBuilder.build()
                chain.proceed(request)
            }

            httpClient.followRedirects(false)
            httpClient.followSslRedirects(false)
            okHttpClient = httpClient.build()
        }
    }


}