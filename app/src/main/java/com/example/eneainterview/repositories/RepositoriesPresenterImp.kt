package com.example.eneainterview.repositories

import android.content.Context
import com.example.eneainterview.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class RepositoriesPresenterImp(var context: Context, var repositoriesView: RepositoriesView) : RepositoriesPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getRepositories() {
        repositoriesView.showProgress()
        compositeDisposable.add(ApiClient.create(context).getRepositories()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object : DisposableSingleObserver<RepositoriesModel>() {
                override fun onSuccess(t: RepositoriesModel) {
                    repositoriesView.hideProgress()
                    repositoriesView.showRepositories(t)
                }

                override fun onError(e: Throwable) {
                    repositoriesView.hideProgress()
                }

            })
        )
    }
}