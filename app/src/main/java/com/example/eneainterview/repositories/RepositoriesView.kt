package com.example.eneainterview.repositories

import com.bookup.app.base.MVPView

interface RepositoriesView : MVPView {
    fun showRepositories(repositoriesModel: RepositoriesModel)
}