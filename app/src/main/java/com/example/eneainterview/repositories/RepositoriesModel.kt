package com.example.eneainterview.repositories

class RepositoriesModel {

    val items: List<RepositoryModel> = ArrayList()

    class RepositoryModel {
        var id: String = ""
        var name: String = ""
        lateinit var owner: Owner
        var score: Float = 0.0f

        class Owner {
            var avatar_url: String = ""
        }
    }
}