package com.example.eneainterview.repositories

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RepositoriesAdapter(private val list: List<RepositoriesModel.RepositoryModel>) :
    RecyclerView.Adapter<RepositoriesAdapter.BookupsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookupsViewHolder {
        return BookupsViewHolder(parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: BookupsViewHolder, position: Int) {
        val bookup: RepositoriesModel.RepositoryModel = list[position]
        holder.bind(bookup)
    }

    class BookupsViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(RepositoryRowView(parent.context)) {

        fun bind(bookup: RepositoriesModel.RepositoryModel) {
            val view: RepositoryRowView = itemView as RepositoryRowView
            view.setDetails(bookup)
        }
    }
}