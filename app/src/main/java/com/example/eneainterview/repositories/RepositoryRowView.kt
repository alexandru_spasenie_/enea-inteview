package com.example.eneainterview.repositories

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.example.eneainterview.R
import kotlinx.android.synthetic.main.repository_row.view.*

class RepositoryRowView : ConstraintLayout {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        inflate(context, R.layout.repository_row, this)
        layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }


    fun setDetails(repository: RepositoriesModel.RepositoryModel) {
        tvTitle.text = repository.name
        Glide
            .with(context)
            .load(repository.owner.avatar_url)
            .circleCrop()
            .into(ivPic)
        tvRating.text = repository.score.toString()
    }
}