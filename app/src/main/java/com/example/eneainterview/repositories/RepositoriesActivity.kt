package com.example.eneainterview.repositories

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.eneainterview.R
import com.example.eneainterview.base.BaseActivity
import kotlinx.android.synthetic.main.activity_scrolling.*
import kotlinx.android.synthetic.main.content_scrolling.*

class RepositoriesActivity : BaseActivity(), RepositoriesView {

    lateinit var repositoriesPresenter: RepositoriesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(toolbar)

        repositoriesPresenter = RepositoriesPresenterImp(this, this)
        repositoriesPresenter.getRepositories()

    }

    override fun showRepositories(repositoriesModel: RepositoriesModel) {
        recyclerViewRepositories.layoutManager = LinearLayoutManager(this)
        val bookupAdapter = RepositoriesAdapter(repositoriesModel.items.sortedBy { it.score })
        recyclerViewRepositories.adapter = bookupAdapter
        bookupAdapter.notifyDataSetChanged()
    }
}
