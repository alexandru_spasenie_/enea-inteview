package com.example.eneainterview.repositories

interface RepositoriesPresenter {
    fun getRepositories()
}